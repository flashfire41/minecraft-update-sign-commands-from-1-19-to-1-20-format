while True:
    original = input("Enter original sign command: ")
    needs_empty_string_added = True
    full_lines_of_text = {1, 2, 3, 4}
    one_line_of_text = {1}
    two_lines_of_text = {1, 2}
    three_lines_of_text = {1, 2, 3}
    lines_of_text = {1, 2, 3, 4}
    no_text1 = False
    no_text2 = False
    no_text3 = False
    no_text4 = False


    if(original.__contains__('Text1') == False):
        print('No Text1')
        no_text1 = True
        lines_of_text.discard(1)
    else:
        if(original.__contains__('Text2') == False):
            print('No Text2')
            no_text2 = True
            lines_of_text.discard(2)
        if(original.__contains__('Text3') == False):
            print('No Text3')
            no_text3 = True
            lines_of_text.discard(3)
        if(original.__contains__('Text4') == False):
            print('No Text4')
            no_text4 = True
            lines_of_text.discard(4)

        if(lines_of_text == full_lines_of_text):
            needs_empty_string_added = False
        elif(lines_of_text == one_line_of_text):
            needs_empty_string_added = False
        elif(lines_of_text == two_lines_of_text):
            needs_empty_string_added = False
        elif(lines_of_text == three_lines_of_text):
            needs_empty_string_added = False
        
        updated = original

        if(needs_empty_string_added):
            print("Adding empty string for missing line")
            if(no_text2):
                if (no_text3 == False):
                    updated = updated.replace('Text3:','\'{"text":""}\',')
                elif(no_text4 == False):
                    updated = updated.replace('Text4:','\'{"text":""}\',')
            if(no_text3):
                if(no_text4 == False):
                    updated = updated.replace('Text4:','\'{"text":""}\',')

        updated = updated.replace('{Text1:', '{front_text:{messages:[')
        updated = updated.replace('Text2:','')
        updated = updated.replace('Text3:','')
        updated = updated.replace('Text4:','')
        updated = updated.replace('}}]\'}','}}]\'}')
        updated = updated.replace('}\'}}','}\']}}}')
        updated = updated.replace('}}]\'}','}}]\']}}')
        updated = updated.replace('}}\'}','}}\']}}')
        print('\n\nUpdated command:\n' + updated)