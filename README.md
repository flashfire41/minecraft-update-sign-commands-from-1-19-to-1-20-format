**What does this script do?**

I wrote a simple Python script where you can input 1.19 format sign commands (/give, /data merge, /setblock, /fill, etc.) and it will output an updated command that will work in 1.20. I'd say it works 99% of the time without any additional tweaking to the output command. No knowledge of Python is necessary. I created the script because it would have been very tedious to manually tweak all of the sign commands for my custom Minecraft mob arena Haphazard Arena.

**What is Haphazard Arena?**

https://www.patreon.com/haphazardarena
https://discord.gg/jTEghkjy
Haphazard Arena is a passion project that has been in development since 2013 as a Minecraft minigame for friends to fight mobs and have fun programming. Eight years and hundreds of command blocks later, the project has evolved to have every trait of our dream mob arena, with custom classes, boss fights, difficulty settings, randomized maps, easter eggs, economy, achievements, and more being added almost every day. Our patreon exists to support the development of the game we love so much, and keep it running for as many people who would like to play it. Updates are extremely frequent and are constantly posted to the Discord server, along with what is in development and planned for the future. Becoming a patron of any tier grants you access to the private beta Java Minecraft server and Discord server, where you can make suggestions, see the development in real time, chat with other players and developers, and of course, battle mobs with your friends! *Haphazard Arena is only available for the Java Edition of Minecraft*

"Haphazard Arena has been a passion project of mine for a very long time, and I am excited to let more people experience this custom Minecraft game mode that I have worked so hard on. I am constantly working on the game to fix bugs and add new features."

- Flashfire4 (Robert)

"If there's one thing I want to emphasize on the page, it's that this is a passion project. We like playing our game as much as we like developing it, and with that mentality, we hope others end up feeling as engrossed as we are."

- Vinity (Alex)
